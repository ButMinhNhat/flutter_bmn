import 'package:flutter/material.dart';
import '../stream/image_stream.dart';
import '../model/image_model.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  GetImageStream imageStream = GetImageStream();
  List<ImageModel> imageArray = [];

  HomePageState() {
    getStreamImage();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
          appBar: AppBar(
            title: Text('App Bar'),
          ),
          body: ListView.builder(
              itemCount: imageArray.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  padding: const EdgeInsets.all(8),
                  // Prevent null value by using replace value
                  child: Image.network(imageArray[index].url ?? ""),
                );
              })),
    );
  }

  getStreamImage() async {
    imageStream.getImage().listen((imageObj) => setState(() {
          imageArray.add(imageObj!);
        }));
  }
}
