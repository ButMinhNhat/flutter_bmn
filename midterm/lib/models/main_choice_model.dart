import 'package:flutter/cupertino.dart';

class MainChoiceItem {
  final IconData icon;
  final String title;
  final String message;
  final Color color;

  MainChoiceItem({
    required this.icon,
    required this.title,
    required this.message,
    required this.color
  });
}
