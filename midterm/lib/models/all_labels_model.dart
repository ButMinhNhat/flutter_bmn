import 'package:flutter/cupertino.dart';

class AllLabels {
  final IconData icon;
  final String title;
  final String messages;

  AllLabels({
    required this.icon,
    required this.title,
    required this.messages,
  });
}
