import 'package:flutter/cupertino.dart';

class BottomSheetItem {
  final String title;
  final IconData icon;

  BottomSheetItem({required this.icon, required this.title});
}