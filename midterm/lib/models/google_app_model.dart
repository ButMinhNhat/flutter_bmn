import 'package:flutter/material.dart';

class GoogleApps {
  final IconData icon;
  final String title;


  GoogleApps({
    required this.icon,
    required this.title,
  });
}