class EmailItem {
  final String title, description, time, content;
  bool favorite;
  bool isRead;

  EmailItem({
    required this.title,
    required this.description,
    required this.content,
    required this.time,
    required this.favorite,
    required this.isRead,
  });
}
