import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:gmail/services/gg_sign_in.dart';
import 'package:gmail/ui/signin_screen.dart';
import 'package:provider/provider.dart';
import 'ui/reply_email_screen.dart';
import 'ui/setting_screen.dart';
import 'ui/join_with_code_screen.dart';
import 'ui/bottom_navigator.dart';
import 'ui/compose_screen.dart';
import 'ui/email_content_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    // Replace with actual values
    options: const FirebaseOptions(
      apiKey: "AIzaSyCRJYywiUjQ2-X-F-v7bZlpFFt5u7wpDn4",
      appId: "1:324107791083:android:f59c048b2a6623bbbdecae",
      messagingSenderId: "324107791083",
      projectId: "login-with-gg-4882c",
    )
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => GoogleSignInProvider(),
        child: MaterialApp(
          title: 'Gmail',
          routes: {
            '/home': (context) => BotNavigationBar(),
            '/email-content': (context) => EmailContentScreen(),
            '/compose-screen': (context) => ComposeScreen(),
            '/join-with-code': (context) => JoinWithCodeScreen(),
            '/setting': (context) => SettingScreen(),
            '/reply': (context) => ReplyEmailScreen(),
            '/sign-in': (context) => SignInScreen(),
          },
          initialRoute: '/home',
        ),
      );
}
