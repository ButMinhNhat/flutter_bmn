import 'package:flutter/material.dart';
import 'package:gmail/models/bottomsheet_model.dart';
import 'models/main_choice_model.dart';
import 'models/all_labels_model.dart';
import 'models/email_item_model.dart';
import 'models/google_app_model.dart';

List<EmailItem> mailList = [
  EmailItem(
    title: 'Flask',
    description: 'This mail is from Flask',
    content:
        'Reply to this email directly or view it on GitLab. You are receving this email because of your account on gitlab.com. ',
    time: '11:25',
    favorite: true,
    isRead: true,
  ),
  EmailItem(
      title: 'Google',
      description: 'This mail is from google',
      content: 'Dummy content for mail app!',
      time: '4:30',
      favorite: false,
      isRead: false),
  EmailItem(
    title: 'Facebook',
    description: 'This mail is from facebook',
    content: 'Dummy content for mail app!',
    time: '4:20',
    favorite: false,
    isRead: true,
  ),
  EmailItem(
    title: 'DinhThi',
    description: 'This mail is from cudinhthi120@gmail.com',
    content: 'Dummy content for mail app!',
    time: '7:20',
    favorite: true,
    isRead: false,
  ),
  EmailItem(
    title: 'VuTran',
    description: 'This mail is from Vũ',
    content: 'Dummy content for mail app!',
    time: '1:00',
    favorite: false,
    isRead: false,
  ),
];

List<AllLabels> allLabelList = [
  AllLabels(
    icon: Icons.star_border,
    title: 'Có gắn dấu sao',
    messages: '1',
  ),
  AllLabels(
    icon: Icons.punch_clock_outlined,
    title: 'Đã tạm ẩn',
    messages: '12',
  ),
  AllLabels(
    icon: Icons.sticky_note_2_outlined,
    title: 'Quan trọng',
    messages: '2',
  ),
  AllLabels(
    icon: Icons.send_sharp,
    title: 'Đã gửi',
    messages: '99',
  ),
  AllLabels(
    icon: Icons.send_and_archive,
    title: 'Đã lên lịch',
    messages: '',
  ),
  AllLabels(
    icon: Icons.forward_to_inbox,
    title: 'Hộp thư đi',
    messages: '81',
  ),
  AllLabels(
    icon: Icons.insert_drive_file_outlined,
    title: 'Thư nháp',
    messages: '99',
  ),
  AllLabels(
    icon: Icons.mail_outline_outlined,
    title: 'Tất cả thư',
    messages: 'Hơn99',
  ),
  AllLabels(
    icon: Icons.warning_amber_outlined,
    title: 'Thư rác',
    messages: '1',
  ),
  AllLabels(
    icon: Icons.restore_from_trash_outlined,
    title: 'Thùng rác',
    messages: '99',
  ),
];

List<GoogleApps> googleAppList = [
  GoogleApps(
    icon: Icons.calendar_today,
    title: 'Lịch',
  ),
  GoogleApps(
    icon: Icons.account_circle_outlined,
    title: 'Danh bạ',
  ),
  GoogleApps(
    icon: Icons.settings_outlined,
    title: 'Cài đặt',
  ),
  GoogleApps(
    icon: Icons.contact_support_outlined,
    title: 'Trợ giúp và phản hồi',
  ),
];

List<MainChoiceItem> mainChoiceList = [
  MainChoiceItem(
    icon: Icons.picture_in_picture_sharp,
    title: 'Chính',
    message: '4 mục mới',
    color: Colors.grey[300]!,
  ),
  MainChoiceItem(
    icon: Icons.supervisor_account_outlined,
    title: 'Mạng xã hội',
    message: '1 mục mới',
    color: Colors.blue[200]!,
  ),
  MainChoiceItem(
    icon: Icons.airplane_ticket_outlined,
    title: 'Quảng cáo',
    message: 'Hơn 99 mục mới',
    color: Colors.green[300]!,
  ),
];

List<BottomSheetItem> bottomSheetList = [
  BottomSheetItem(
    icon: Icons.link,
    title: 'Nhận dường liên kết của cuộc gọi để chia sẻ',
  ),
  BottomSheetItem(
    icon: Icons.videocam_outlined,
    title: 'Bắt đầu cuộc gọi ngay tức thì',
  ),
  BottomSheetItem(
    icon: Icons.calendar_today_rounded,
    title: 'Lên lịch trong Lich Google',
  ),
  BottomSheetItem(
    icon: Icons.close,
    title: 'Đóng',
  ),
];

String emailCC = 'vuonggiahao@gmail.com, luan@gmail.com';
String emailBCC = 'anhvu@gmail.com';
String emailRep = '[FLasky] Confirm Your Account [27-04-2022 11:25:07]';
String emailContent = 'Đây là 1 nội dung mẫu';