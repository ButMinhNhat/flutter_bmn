import 'package:flutter/material.dart';
import 'home_screen.dart';
import 'meeting_room_screen.dart';

class BotNavigationBar extends StatefulWidget {
  static const route = '/';

  const BotNavigationBar({Key? key}) : super(key: key);

  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<BotNavigationBar> {
  int currentIndex = 0;
  List items = [
    {
      'label': 'Thư',
      'icon': Icons.mail_rounded,
    },
    {
      'label': 'Họp mặt',
      'icon': Icons.videocam_outlined,
    },

  ];
  final List screens = [
    HomeScreen(),
    MeetingRoomScreen(),
  ];

  void onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.grey[200],
        onTap: onTap,
        elevation: 0,
        currentIndex: currentIndex,
        unselectedIconTheme: IconThemeData(
          color: Colors.black,
        ),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        items: items
            .map((item) => BottomNavigationBarItem(
            icon: Icon(item['icon']), label: item['label']))
            .toList(),
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.black45,
      ),
    );
  }
}
