import 'package:flutter/material.dart';

class JoinWithCodeScreen extends StatelessWidget {
  static const route = '/join-with-code';

  const JoinWithCodeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text(
          'Tham gia bằng mã',
          style: TextStyle(fontWeight: FontWeight.normal),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16, top: 20),
            child: Text(
              "Tham gia",
              style: TextStyle(color: Colors.grey[500]),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text('Nhập mã do người tổ chức cuộc họp cung cấp'),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: TextField(
              autofocus: true,
              cursorColor: Colors.blue[900],
              cursorHeight: 20,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey[350],
                hintText: 'Ví dụ: abc-mnop-xyz',
                hintStyle: TextStyle(color: Colors.grey),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
