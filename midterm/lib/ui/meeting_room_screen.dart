import 'package:flutter/material.dart';
import '../data.dart';
import 'join_with_code_screen.dart';
import 'components/drawer.dart';

class MeetingRoomScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  MeetingRoomScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: MyDrawer(),
      appBar: AppBar(
        elevation: 0,
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        title: Text(
          'Họp mặt',
          style: TextStyle(fontWeight: FontWeight.normal),
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            _scaffoldKey.currentState?.openDrawer();
          },
          icon: Icon(
            Icons.menu,
            color: Colors.black,
            size: 24,
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.all(12),
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://meta.vn/Data/image/2021/09/22/anh-meo-cute-de-thuong-dang-yeu-43.jpg'),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.only(right: 8, left: 16),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      textStyle:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                      primary: Colors.white,
                      backgroundColor: Colors.blue[900],
                      side: BorderSide.none,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    onPressed: () {
                      showModalBottomSheet(
                        context: context,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(20),
                          ),
                        ),
                        builder: (BuildContext context) {
                          return Container(
                            height: MediaQuery.of(context).size.height * 0.35,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.vertical(
                                top: Radius.circular(20),
                              ),
                            ),
                            child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: bottomSheetList.length,
                              itemBuilder: (context, index) => Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(right: 16.0),
                                      child: Icon(bottomSheetList[index].icon),
                                    ),
                                    Text(bottomSheetList[index].title, style: TextStyle(fontSize: 16),),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    },
                    child: Text('Cuộc họp mới'),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.only(right: 16, left: 8),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      textStyle:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                      primary: Colors.blue[900],
                      backgroundColor: Colors.white,
                      side: BorderSide(width: 1, color: Colors.grey[600]!),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(JoinWithCodeScreen.route);
                    },
                    child: Text('Tham gia bằng mã'),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.15,
          ),
          Image.asset(
            'assets/img.png',
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 16),
            width: MediaQuery.of(context).size.width * 0.65,
            child: Text(
              'Tạo 1 đường liên kết mà bạn có thể chia sẻ',
              style: TextStyle(fontSize: 22),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.55,
            child: Text(
              'Nhấn vào cuộc họp mới để tạo một đường liên kết mà bạn có thể gửi cho những người mình muốn họp cùng',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14),
            ),
          ),
        ],
      ),
    );
  }
}
