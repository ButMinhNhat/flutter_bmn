import 'package:flutter/material.dart';
import '../setting_screen.dart';
import '../../data.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: MediaQuery.of(context).size.width -90, // Set width for drawer equal to screen width - 90;
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Drawer Title
              Padding(
                padding: const EdgeInsets.only(bottom: 32, left: 32),
                child: Text(
                  'Gmail',
                  style: TextStyle(
                      color: Colors.red[800],
                      fontSize: 22,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Divider(
                height: 1,
                color: Colors.black,
              ),
              Container(
                margin: EdgeInsets.only(top: 8, bottom: 8, right: 8),
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.lightBlue[100],
                ),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 12, right: 28),
                      child: Icon(Icons.picture_as_pdf_outlined),
                    ),
                    Text(
                      'Tất cả hộp thư đến',
                      style: TextStyle(
                          fontWeight: FontWeight.w500, fontSize: 16),
                    )
                  ],
                ),
              ),
              Divider(
                height: 1,
                color: Colors.black,
              ),

              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: mainChoiceList.length,
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 12, right: 28),
                          child: Icon(mainChoiceList[index].icon),
                        ),
                        Expanded(
                          child: Text(
                            mainChoiceList[index].title,
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            color: mainChoiceList[index].color,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Text(
                            mainChoiceList[index].message,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              // All Labels Title
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 32),
                child: Text('Tất cả nhãn dán'),
              ),

              // All Labels ListView
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: allLabelList.length,
                itemBuilder: (context, index) => Container(
                  padding: EdgeInsets.all(16),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 12, right: 28),
                        child: Icon(allLabelList[index].icon),
                      ),
                      Expanded(
                        child: Text(
                          allLabelList[index].title,
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 16),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(4),
                        child: Text(
                          allLabelList[index].messages,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 32),
                child: Text('Các ứng dụng của Google'),
              ),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: googleAppList.length,
                itemBuilder: (context, index) => GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushNamed(SettingScreen.route);
                  },
                  child: Container(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 12, right: 28),
                          child: Icon(googleAppList[index].icon),
                        ),
                        Expanded(
                          child: Text(
                            googleAppList[index].title,
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

