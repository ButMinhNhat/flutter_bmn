import 'package:flutter/material.dart';
import 'package:gmail/ui/signin_screen.dart';
import 'reply_email_screen.dart';

class EmailContentScreen extends StatelessWidget {
  static const route = '/email-content';

  const EmailContentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        actions: [
          SizedBox(
            width: 300,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 12),
                  child: Icon(
                    Icons.archive_outlined,
                    size: 24,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12, right: 12),
                  child: Icon(
                    Icons.restore_from_trash_outlined,
                    size: 24,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12),
                  child: Icon(
                    Icons.email_outlined,
                    size: 24,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8, left: 8),
                  child:  PopupMenuButton(
                    color: Colors.grey[100],
                    icon: Icon(Icons.more_vert_outlined),
                    itemBuilder: (context) => [
                      PopupMenuItem(child: Text('Di chuyển tới'), value: 1,),
                      PopupMenuItem(child: Text('Tạm ẩn'), value: 2,),
                      PopupMenuItem(child: Text('Thay đổi nhãn dán'), value: 3,),
                      PopupMenuItem(child: Text('Đánh dấu là quan trọng'), value: 4,),
                      PopupMenuItem(child: Text('Bỏ qua'), value: 5,),
                      PopupMenuItem(child: Text('In'), value: 6,),
                      PopupMenuItem(child: Text('H.nguyên về t.động đặt cỡ'), value: 7,),
                      PopupMenuItem(child: Text('Báo cáo spam'), value: 8,),
                      PopupMenuItem(child: Text('Thêm vào Tasks'), value: 9,),
                      PopupMenuItem(child: Text('Trợ giúp và phản hồi'), value: 10,),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              // Email Title
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 16, top: 16, bottom: 16),
                      child: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                              text:
                                  '[FLasky] Confirm Your Account [27-04-2022 11:25:07]',
                            ),
                            WidgetSpan(
                              child: Container(
                                margin: EdgeInsets.only(left: 16, top: 6),
                                padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Text(
                                  'Hộp thư đến',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(Icons.star_outline),
                  ),
                ],
              ),

              // Sender Info
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(
                          'https://meta.vn/Data/image/2021/09/22/anh-meo-cute-de-thuong-dang-yeu-43.jpg'),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'ButMinhNhat(@ButMinhNhat) hello ',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: Text('19 thg 4'),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text('tới tôi'),
                            Icon(Icons.arrow_drop_down),
                          ],
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(ReplyEmailScreen.route);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Icon(Icons.undo_outlined),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8, left: 8),
                    child:  PopupMenuButton(
                      color: Colors.grey[100],
                      icon: Icon(Icons.more_vert_outlined),
                      itemBuilder: (context) => [
                        PopupMenuItem(child: Text('Trả lời tất cả'), value: 1,),
                        PopupMenuItem(child: Text('Chuyển tiếp'), value: 2,),
                        PopupMenuItem(child: Text('Thêm dấu sao'), value: 3,),
                        PopupMenuItem(child: Text('In'), value: 4,),
                        PopupMenuItem(child: Text('Đánh dấu là chưa đọc từ đây'), value: 5,),
                        PopupMenuItem(child: Text('Chặn "Discord"'), value: 6,),
                      ],
                    ),
                  ),
                ],
              ),

              // Email Header
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text('Tựa đề'),
              ),

              // Email content
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                    'Reply to this email directly or view it on GitLab. You are receving this email because of your account on gitlab.com. If you like to recevie fewer emails, you can unsubscribe from this thread or adjust your notification settings.'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
