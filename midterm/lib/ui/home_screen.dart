import 'package:flutter/material.dart';
import 'package:gmail/ui/signin_screen.dart';
import 'components/drawer.dart';
import '../data.dart';
import 'compose_screen.dart';
import 'email_content_screen.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: MyDrawer(),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Search Bar
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              height: 50,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(50),
              ),
              child: Row(
                children: [
                  // Menu button open Drawer
                  IconButton(
                    onPressed: () {
                      _scaffoldKey.currentState?.openDrawer();
                    },
                    icon: Icon(
                      Icons.menu,
                      color: Colors.black,
                      size: 24,
                    ),
                  ),
                  // Search Field
                  Expanded(
                    flex: 1,
                    child: TextField(
                      cursorColor: Colors.blue[800],
                      cursorWidth: 2,
                      decoration: InputDecoration(
                        hintText: 'Tìm kiếm trong thư',
                        focusedBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        isDense: false,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => showDialog(
                      context: context,
                      builder: (BuildContext content) => Dialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        alignment: Alignment.topCenter,
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.45,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 16,
                                  right: 16,
                                  top: 8,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Icon(
                                        Icons.close,
                                        size: 16,
                                      ),
                                    ),
                                    Expanded(
                                      child: Image.asset(
                                        'assets/google-icon.png',
                                        height: 22,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 16,
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(height: 12),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 16),
                                    child: SizedBox(
                                      width: 30,
                                      height: 30,
                                      child: CircleAvatar(
                                        backgroundImage: NetworkImage(
                                          'https://meta.vn/Data/image/2021/09/22/anh-meo-cute-de-thuong-dang-yeu-43.jpg',
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Bút Minh Nhật',
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Text(
                                            'butminhnhat@gmail.com',
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(fontSize: 10),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Text(
                                      '99+',
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                height: 32,
                                padding: EdgeInsets.symmetric(horizontal: 2),
                                margin: EdgeInsets.symmetric(
                                    horizontal: 32, vertical: 4),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(16),
                                  border: Border.all(
                                    width: 1,
                                    color: Colors.grey[300]!,
                                  ),
                                ),
                                child: TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Quản Lý Tài Khoản Google của bạn',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ),
                              SizedBox(height: 16),
                              Container(
                                height: 1,
                                color: Colors.grey[350],
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pushNamed(SignInScreen.route);
                                },
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 12),
                                      child: Icon(Icons.person_add_alt, color: Colors.black, size: 14,),
                                    ),
                                    Text('Thêm một tài khoản khác', style: TextStyle(color: Colors.black, fontSize: 13),),
                                  ],
                                ),
                              ),
                              TextButton(
                                onPressed: () {

                                },
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 12),
                                      child: Icon(Icons.person_search_outlined, color: Colors.black, size: 14,),
                                    ),
                                    Text('Quản lý các tài khoản trên thiết bị này', style: TextStyle(color: Colors.black, fontSize: 13),),
                                  ],
                                ),
                              ),
                              Container(
                                height: 1,
                                color: Colors.grey[350],
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(16),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Chính sách quyền riêng tư',
                                        style: TextStyle(fontSize: 10),
                                      ),
                                      SizedBox(
                                        child: Container(
                                          height: 3,
                                          width: 3,
                                          decoration: BoxDecoration(
                                              color: Colors.black,
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                        ),
                                      ),
                                      Text(
                                        'Điều khoản dịch vụ',
                                        style: TextStyle(fontSize: 10),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                          'https://meta.vn/Data/image/2021/09/22/anh-meo-cute-de-thuong-dang-yeu-43.jpg',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            // Body title
            Padding(padding: EdgeInsets.all(16), child: Text('Chính')),
            // List Email
            Expanded(
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: mailList.length,
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed(EmailContentScreen.route);
                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 16, bottom: 32),
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              'https://meta.vn/Data/image/2021/09/22/anh-meo-cute-de-thuong-dang-yeu-43.jpg'),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      mailList[index].title,
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: mailList[index].isRead
                                            ? FontWeight.normal
                                            : FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    mailList[index].time,
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: mailList[index].isRead
                                          ? FontWeight.normal
                                          : FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                              Text(
                                mailList[index].description,
                                style: TextStyle(
                                  fontWeight: mailList[index].isRead
                                      ? FontWeight.normal
                                      : FontWeight.bold,
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      mailList[index].content,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Icon(
                                    mailList[index].favorite
                                        ? Icons.star
                                        : Icons.star_border,
                                    color: mailList[index].favorite
                                        ? Colors.blue
                                        : Colors.black,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),

      // Compose Button
      floatingActionButton: GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(ComposeScreen.route);
        },
        child: Container(
          padding: EdgeInsets.only(left: 14, right: 14, top: 10, bottom: 10),
          decoration: BoxDecoration(
            color: Colors.lightBlue[200],
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 5,
                color: Colors.black26,
                offset: Offset(2, 0),
              )
            ],
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 6, bottom: 6, right: 8),
                child: Icon(
                  Icons.edit_outlined,
                  size: 18,
                  color: Colors.black,
                ),
              ),
              Text(
                'Soạn thư',
                style: TextStyle(color: Colors.black, fontSize: 13),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ButtonInDialog extends StatelessWidget {
  final String title;
  final IconData icon;

  const ButtonInDialog({
    Key? key,
    required this.title,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      color: Colors.white,
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {},
        child: Row(
          children: [
            Icon(
              icon,
              color: Colors.black,
              size: 16,
            ),
            SizedBox(width: 16),
            Expanded(
              child: Text(
                title,
                style: TextStyle(color: Colors.black, fontSize: 12),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
