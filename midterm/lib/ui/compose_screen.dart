import 'package:flutter/material.dart';

class ComposeScreen extends StatelessWidget {
  static const route = '/compose-screen';

  const ComposeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text(
          'Soạn thư',
          style: TextStyle(fontWeight: FontWeight.normal),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(Icons.attachment_outlined),
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: Icon(Icons.send_outlined),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Icon(Icons.more_vert_outlined),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1, color: Colors.grey[200]!),
              )
            ),
            height: 50,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Từ'),
                ),
                Expanded(
                  child: TextField(
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                      focusedErrorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1, color: Colors.grey[200]!),
                )
            ),
            height: 50,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Tới'),
                ),
                Expanded(
                  child: TextField(
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1, color: Colors.grey[200]!),
                )
            ),
            height: 50,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Cc'),
                ),
                Expanded(
                  child: TextField(
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1, color: Colors.grey[200]!),
                )
            ),
            height: 50,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Bcc'),
                ),
                Expanded(
                  child: TextField(
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1, color: Colors.grey[200]!),
                )
            ),
            height: 50,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Chủ đề'),
                ),
                Expanded(
                  child: TextField(
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text('Soạn email'),
              ),
              Expanded(
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  minLines: 1,
                  maxLines: 8,
                  cursorHeight: 20,
                  cursorWidth: 1.5,
                  cursorColor: Colors.blue[900],
                  decoration: InputDecoration(
                      focusedErrorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
