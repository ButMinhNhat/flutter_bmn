import 'package:flutter/material.dart';

import '../data.dart';

class ReplyEmailScreen extends StatelessWidget {
  static const route = '/reply';

  const ReplyEmailScreen({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                    child: Icon(Icons.arrow_back),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    'Trả lời',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: PopupMenuButton(
                      offset: Offset(-65,0),

                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.35
                      ),
                      color: Colors.grey[100],
                      icon: Icon(Icons.arrow_drop_down_sharp),
                      itemBuilder: (context) => [
                        PopupMenuItem(child: Text('Trả lời', style: TextStyle(fontSize: 15),), value: 1,),
                        PopupMenuItem(child: Text('Trả lời tất cả',style: TextStyle(fontSize: 15),), value: 2,),
                        PopupMenuItem(child: Text('Chuyển tiếp',style: TextStyle(fontSize: 15),), value: 3,),
                      ],
                    ),
                  ),
                ),
                Icon(Icons.attachment_outlined),
                Padding(
                  padding: const EdgeInsets.only(right: 8, left: 18),
                  child: Icon(
                    Icons.send_outlined,
                    color: Colors.blue[900],
                  ),
                ),
                Icon(Icons.more_vert),
              ],
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text('Từ'),
              ),
              Expanded(
                child: TextFormField(
                  cursorHeight: 20,
                  cursorWidth: 1.5,
                  cursorColor: Colors.blue[900],
                  initialValue: 'butminhnhat@gmail.com',
                  decoration: InputDecoration(
                    focusedErrorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: BorderSide(width: 1, color: Colors.grey[200]!),
            )),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Tới'),
                ),
                Container(
                  height: 35,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(width: 0.8, color: Colors.grey[400]!)),
                  child: TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                    ),
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(2),
                          width: 30,
                          height: 30,
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                              'https://meta.vn/Data/image/2021/09/22/anh-meo-cute-de-thuong-dang-yeu-43.jpg',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Text(
                            'Discord',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 11,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: TextField(
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                      focusedErrorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: BorderSide(width: 1, color: Colors.grey[200]!),
            )),
            height: 50,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Cc'),
                ),
                Expanded(
                  child: TextField(
                    controller: TextEditingController(text: emailCC),
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: BorderSide(width: 1, color: Colors.grey[200]!),
            ),
            ),
            height: 50,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Bcc'),
                ),
                Expanded(
                  child: TextField(
                    controller: TextEditingController(text: emailBCC),
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    decoration: InputDecoration(
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: BorderSide(width: 1, color: Colors.grey[200]!),
            )),
            height: 70,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Re:'),
                ),
                Expanded(
                  child: TextField(
                    controller: TextEditingController(text: emailRep),
                    cursorHeight: 20,
                    cursorWidth: 1.5,
                    cursorColor: Colors.blue[900],
                    maxLines: 2,
                    decoration: InputDecoration(
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: TextField(
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              minLines: 1,
              maxLines: 8,
              controller: TextEditingController(text: emailContent),
              cursorHeight: 20,
              cursorWidth: 1.5,
              cursorColor: Colors.blue[900],
              decoration: InputDecoration(
                focusedErrorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                hintText: 'Soạn mail',
                hintStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
