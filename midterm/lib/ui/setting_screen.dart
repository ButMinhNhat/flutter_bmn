import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gmail/services/gg_sign_in.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatelessWidget {
  static const route = '/setting';

  SettingScreen({Key? key}) : super(key: key);

  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        title: Text('Cài đặt'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: PopupMenuButton(
              color: Colors.grey[100],
              icon: Icon(Icons.more_vert_outlined),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text('Quản lý tải khoản'),
                  value: 1,
                ),
                PopupMenuItem(
                  child: Text('Xóa nhật ký tìm kiếm'),
                  value: 2,
                ),
                PopupMenuItem(
                  child: Text('Xóa sự phê duyệt ảnh'),
                  value: 3,
                ),
                PopupMenuItem(
                  child: Text('Trợ giúp và phản hồi'),
                  value: 4,
                ),
              ],
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: const [
                _buildItem(
                  title: 'Cài đặt chung',
                ),
                _buildItem(
                  title: 'butminhnhat@gmail.com',
                ),
                _buildItem(
                  title: 'Thêm tài khoản',
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            height: 40,
            width: double.infinity,
            child: ElevatedButton(
              onPressed: auth.currentUser == null
                  ? null
                  : () async {
                      final provider = Provider.of<GoogleSignInProvider>(
                          context,
                          listen: false);
                      await provider.logOut();
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          backgroundColor: Colors.green,
                          padding: EdgeInsets.all(20),
                          content: Text("Đăng xuất thành công"),
                        ),
                      );
                      Navigator.of(context).pop();
                    },
              child: Text(
                'Log Out'.toUpperCase(),
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _buildItem extends StatelessWidget {
  final String title;

  const _buildItem({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextButton(
        onPressed: () {},
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style:
                TextStyle(color: Colors.black, fontWeight: FontWeight.normal),
          ),
        ),
      ),
    );
  }
}
