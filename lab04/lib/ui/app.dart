import 'package:flutter/material.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  final List<Map<String, dynamic>> itemsList = <Map<String, dynamic>>[
    {'name': 'Item 1', 'price': '100.000'},
    {'name': 'Item 2', 'price': '200.000'},
    {'name': 'Item 3', 'price': '300.000'},
    {'name': 'Item 4', 'price': '400.000'},
    {'name': 'Item 5', 'price': '500.000'},
    {'name': 'Item 6', 'price': '600.000'},
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('App Bar'),
        ),
        body: GridView.builder(
            padding: const EdgeInsets.all(20),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20),
            itemCount: itemsList.length,
            itemBuilder: (BuildContext ctx, index) {
              return Container(
                alignment: Alignment.center,
                child: FractionallySizedBox(
                    widthFactor: 1,
                    child: Container(
                      width: 50,
                      height: 200,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey,
                          width: 5,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text("${itemsList[index]['name']}",
                          style: const TextStyle(fontSize: 20)),
                    )),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15)),
              );
            }),
        // ListView.separated(
        //   padding: const EdgeInsets.all(8),
        //   itemCount: itemsList.length,
        //   itemBuilder: (BuildContext context, int index) {
        //     return (
        //       FractionallySizedBox(
        //         widthFactor: 0.5,
        //         child: Container(
        //           padding: const EdgeInsets.symmetric(
        //               horizontal: 16.0, vertical: 8.0),
        //           child: Container(
        //             width: 50,
        //             height: 200,
        //             decoration: BoxDecoration(
        //               border: Border.all(
        //                 color: Colors.black,
        //                 width: 5,
        //               ),
        //               borderRadius: BorderRadius.circular(10),
        //             ),
        //             child: Center(
        //                 child: Text("MyText", style: TextStyle(fontSize: 20))),
        //           ),
        //         )
        //       )
        //     );
        //   },
        //   separatorBuilder: (BuildContext context, int index) => const Divider(),
        // ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Trang chủ',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label: 'Danh mục',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat),
              label: 'Chat',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Cá nhân',
            ),
          ],
        ),
      ),
    );
  }
}
