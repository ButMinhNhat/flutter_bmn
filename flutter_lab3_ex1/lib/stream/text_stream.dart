import 'package:flutter/material.dart';

class TextStream {
  Stream<String> getTexts() async* {
    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      return "Hello, This is a stream message";
    });
  }
}
