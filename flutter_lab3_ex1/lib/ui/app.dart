import 'package:flutter/material.dart';
import '../stream/text_stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  TextStream textStream = TextStream();
  List<String> textArray = [];

  HomePageState() {
    getStreamText();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
        appBar: AppBar(
          title: Text('App Bar'),
        ),
        body: ListView.builder(
          itemCount: textArray.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
                padding: const EdgeInsets.all(8),
                // Prevent null value by using replace value
                child: Text(textArray[index]));
          },
        ),
      ),
    );
  }

  getStreamText() async {
    textStream.getTexts().listen((eventText) {
      setState(() {
        print(eventText);
        textArray.add(eventText);
      });
    });
  }
}
