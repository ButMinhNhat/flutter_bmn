import 'dart:convert';
import 'package:flutter/material.dart';
import '../model/course.dart';
import '../sakai_services.dart';
import '/ui/courses.dart';
import '../validation.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  static const route = '/';

  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final serverController = TextEditingController();
  List course = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Trang đăng nhập'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(12),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
                decoration: InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: 'Username',
                    hintText: 'Email@gmail.com'),
                validator: validateEmail,
              ),
              TextFormField(
                obscureText: true,
                controller: passwordController,
                decoration: InputDecoration(
                  icon: Icon(Icons.password),
                  labelText: 'Password',
                ),
                validator: validatePassword,
              ),
              TextFormField(
                controller: serverController,
                decoration: InputDecoration(
                  icon: Icon(Icons.link),
                  labelText: 'Server',
                ),
                validator: validateServer,
              ),
              Container(
                margin: EdgeInsets.only(top: 12),
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.green, primary: Colors.white),
                    onPressed: validate,
                    child: Text('Đăng nhập')),
              )
            ],
          ),
        ),
      ),
    );
  }

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      checkAndGetCourse();
    }
  }

  Future checkAndGetCourse() async {
    var sakaiServices = SakaiService(sakaiUrl: serverController.text);
    await sakaiServices.authenticate(
        emailController.text, passwordController.text);
    http.Response response = await sakaiServices.getSites();
    if (sakaiServices.token != null) {
      print(sakaiServices.token);
      setState(() {
        var jsonOjb = jsonDecode(response.body);
        if (jsonOjb != null) {
          Iterable i = jsonOjb["site_collection"];
          course = List<Course>.from(i.map((model) => Course.fromJson(model)));
        }
      });
      Navigator.of(context).pushNamed(Courses.route, arguments: course);
    }
    else {
      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          content: Text('Sai email hoặc mật khẩu'),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }
}
