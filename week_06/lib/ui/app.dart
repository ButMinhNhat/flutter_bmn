import 'package:flutter/material.dart';
import '../validation/mixin_validation.dart';
import '../bloc/bloc.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Log Me In!',
      home: Scaffold(
        appBar: AppBar(title: Text('Login'),),
        body: LoginScreen(),
      )
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }

}

class LoginScreenState extends State<StatefulWidget> with CommonValidator {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  final bloc = Bloc();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 10),),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 10),),
            loginButton(),
          ],
        ),
      )
    );
  }

  Widget emailField() {
    return StreamBuilder(
        stream: bloc.streamEmail,
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          return TextFormField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                icon: Icon(Icons.person),
                labelText: 'Email address',
                errorText: snapshot.hasError ? snapshot.error as String : null
            ),
            // validator: validateEmail,
            onChanged: (value) {
              bloc.changeEmail(value);
            },
            onSaved: (value) {
              email = value as String;
            },
          );
        });
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.password),
        labelText: 'Password'
      ),
      validator: validatePassword,
      onSaved: (String? value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
            print('Save $email, $password');
          }
        },
        child: Text('Login')
    );
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
}