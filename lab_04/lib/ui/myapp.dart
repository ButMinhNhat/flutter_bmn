import 'package:flutter/cupertino.dart';

class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Map<String, String>> itemLists = [
      { 
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/a0/0d/90/bab67b6da67117f40538fc54fb2dcb5e.png",
        "name": "Đi chợ online"
      },
      { 
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/ff/20/4a/0a7c547424f2d976b6012179ed745819.png",
        "name": "Mua bán ASA/XU"
      },
      {
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/73/50/e1/83afc85db37c472de60ebef6eceb41a7.png",
        "name": "Mã giảm giá"
      },
      {
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/ef/ae/82/f40611ad6dfc68a0d26451582a65102f.png",
        "name": "Bảo hiểm Tiki360"
      },
      {
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/73/e0/7d/af993bdbf150763f3352ffa79e6a7117.png",
        "name": "Dịch vụ & Tiện ích"
      },
      {
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/99/29/ff/cea178635fd5a24ad01617cae66c065c.png",
        "name": "Giảm đến 50%"
      },
      {
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/52/50/73/0788d5207ec8b82e05859dfe953a4327.png",
        "name": "Hoàn tiền 15%"
      },
      {
        "src": "https://salt.tikicdn.com/cache/w100/ts/upload/4a/b2/c5/b388ee0e511889c83fab1217608fe82f.png",
        "name": "Ưu đãi thanh toán"
      }
    ];

    return Container(
      padding: const EdgeInsets.all(10),
      height: 100,
      child: 
        ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: itemLists.length,
          itemBuilder: (BuildContext ctx, index) {
            return Container(
              margin: const EdgeInsets.only( right: 20 ),
              width: 70,
              child:  Column(
                children: <Widget>[
                  Image.network(
                    itemLists[index]["src"] as String,
                    width: 40,
                    height: 40,
                  ),
                  Text(
                    itemLists[index]["name"] as String,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  )
                ],
              )
            ); 
          }
        )
    );
  }
}