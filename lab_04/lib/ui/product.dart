import 'package:flutter/material.dart';

class Product extends StatelessWidget {
  const Product({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> itemsList = <Map<String, dynamic>>[
      {
        'name': 'Tinh chất dưỡng trắng da chiết xuất trà xanh cấp ẩm se khít lỗ chân lông và chống lão hóa Dewytree Miracle Pore Minimizing Serum Mini Size 8g', 
        'price': '63.000', 
        'src': "https://salt.tikicdn.com/cache/400x400/ts/product/45/5e/90/89a963f2d3c37a8d99abad8986f9a982.jpg"
      },
      {
        'name': 'Điện Thoại Samsung Galaxy S20 FE - Hàng Chính Hãng', 
        'price': '10.090.000', 
        'src': "https://salt.tikicdn.com/cache/200x200/ts/product/19/5e/21/e9545516e51437aa3266c8a684c83f1d.jpg"
      },
      {
        'name': 'Điện thoại Samsung Galaxy M51 - Hàng Chính Hãng', 
        'price': '6.690.000', 
        'src': "https://salt.tikicdn.com/cache/200x200/ts/product/74/58/59/17e2b2233717d5a82e7a17464f0947d1.jpg"
      },
      {
        'name': 'Điện Thoại Samsung Galaxy M22 (6GB/128GB) - Hàng chính hãng', 
        'price': '4.290.000', 
        'src': "https://salt.tikicdn.com/cache/200x200/ts/product/7f/60/5d/3a8b3981d83f08f8216602f18799192a.jpg"
      },
      {
        'name': 'Ghế Xoay Văn Phòng Làm Việc Công Thái Học Deli - Tay Gập Thông Minh, Có Ngả Lưng, Lưới Thoáng Khí, Chân Xoay Tiện Ích, Đệm Ghế Êm Aí - Phù Hợp Học Sinh, Văn Phòng, Game Thủ, Gaming - Hàng Chính Hãng - E4928 / E4929 / E4925 / E4926G', 
        'price': '699.000', 
        'src': "https://salt.tikicdn.com/cache/200x200/ts/product/a2/e7/82/d6a36211d55e01978591339401e44083.jpg"
      },
      {
        'name': 'Nước Rửa Chén Sunlight Chanh Công Nghệ Mới Dạng Túi', 
        'price': '21.000', 
        'src': "https://salt.tikicdn.com/cache/200x200/ts/product/ee/e5/2a/40d0dc89c5621b6003ba06eb47147bfc.JPG"
      },
    ];

    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Gợi Ý Hôm Nay",
            style: TextStyle( fontSize: 16 )
          ),
          const SizedBox( height: 10),
          GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: MediaQuery.of(context).size.width / MediaQuery.of(context).size.height,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20
              ),
            itemCount: itemsList.length,
            itemBuilder: (BuildContext ctx, index) {
              return Container(
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.network(itemsList[index]["src"]),
                    Padding(
                      padding: const EdgeInsets.only( left: 10, right: 10),
                      child: Text(
                        itemsList[index]["name"],
                        textAlign: TextAlign.left,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle( fontSize: 14 ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only( left: 10, right: 10, top: 10),
                      child: Text(
                        '${itemsList[index]["price"]} đ',
                        textAlign: TextAlign.left,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle( fontSize: 16, fontWeight: FontWeight.bold ),
                      ),
                    )
                  ],
                )
              );
            }
          )
        ]
      )
    );
  }
}
