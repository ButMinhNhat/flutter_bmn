import 'package:flutter/material.dart';
import 'package:lab_04/ui/myapp.dart';
import 'package:lab_04/ui/product.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
        resizeToAvoidBottomInset: false, 
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.lightBlue,
          leading: const AppbarLeading(),
          title: const AppbarTitle(),
          actions: const [
            AppbarAction1(),
            AppbarAction2(),
          ],
          bottom: AppBar(
            backgroundColor: Colors.lightBlue,
            elevation: 0,
            title: const SearchItemField(),
          ),
        ),
        body: Container(
          alignment: Alignment.center,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const <Widget>[
                MyApp(),
                Product()
              ],
            ),
          )
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Trang chủ',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label: 'Danh mục',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat),
              label: 'Chat',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Cá nhân',
            ),
          ],
        ),
      ),
    );
  }
}

class AppbarLeading extends StatelessWidget {
  const AppbarLeading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      child: Text(
        'freeship'.toUpperCase(),
        style: const TextStyle(fontSize: 10, fontWeight: FontWeight.w800),
      ),
      alignment: Alignment.centerRight,
    );
  }
}

class AppbarTitle extends StatelessWidget {
  const AppbarTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String homeScreenTitle = 'Tiki';
    return Text(homeScreenTitle);
  }
}

class AppbarAction1 extends StatelessWidget {
  const AppbarAction1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.of(context).pushNamed("/");
      },
      icon: const Icon(Icons.notifications_active),
    );
  }
}

class AppbarAction2 extends StatelessWidget {
  const AppbarAction2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.of(context).pushNamed("/");
      },
      icon: const Icon(Icons.shopping_cart_outlined),
    );
  }
}

class SearchItemField extends StatelessWidget {
  const SearchItemField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String hintText = 'Bạn tìm gì hôm nay';

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 12),
      color: Colors.lightBlue,
      height: 60,
      child: TextField(
        cursorWidth: 1,
        cursorColor: Colors.black,
        maxLines: 1,
        textAlignVertical: TextAlignVertical.center,
        autofocus: false,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: const Icon(Icons.search),
          contentPadding: EdgeInsets.zero,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(18),
          ),
          hintText: hintText,
        ),
      ),
    );
  }
}

